This module exposes a Rest API resource for encryption and decryption of input string. You can provide authentication and other configuration in Rest UI screen. you can see below Resource in Rest UI configuration screen.

Rest Encrypt Decrypt

you can enable this resource and usage is provided below.


<strong>Decrypt request</strong>

provide action value as decrypt for decryption.  encrypt_profile is the machine name of your encryption profile.input_text Is the input need to be encrypted or decrypted.

Request url –  http://your-project.com/rest/api/encrypt-decrypt/items?_format=json

Request body

{"encrypt_profile":"encrypt_profile","action":"decrypt","input_text":"def50200a999e41579b33e98b4145b042599c97d144d8f7463fb7d93c49e76395b2406a2bdf909d3f32bbcc19f5149fcb80f7c994e21712e065e08540c79b16f2e7d7f07757202eb5da9a68dcff1570d808f090f3c0f4a4c911bf96a16"}

Response

{
 
    "message": "success",
 
    "encrypted_string": "",
 
    "decrypted_string": "nadeem-is"
 
}

<strong>Encrypt request</strong>

Request url –  http://your-project.com/rest/api/encrypt-decrypt/items?_format=json

Request body
{"encrypt_profile":"encrypt_profile","action":"decrypt","input_text":"nadeem-is"}

Response

{
 
    "message": "success",
 
    "encrypted_string": " def50200a999e41579b33e98b4145b042599c97d144d8f7463fb7d93c49e76395b2406a2bdf909d3f32bbcc19f5149fcb80f7c994e21712e065e08540c79b16f2e7d7f07757202eb5da9a68dcff1570d808f090f3c0f4a4c911bf96a16",
 
    "decrypted_string": ""
 
}

This module has dependencies on below modules.


  Restful web services
  Encrypt
  keys


